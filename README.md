![PyThia full Logo](logo/logo_full_transparent.png)

# PyThia Logo Repository

Please use the standard logo (`logo/`) if possible.
Always use the logo in a proper fashion:
  - Make sure the logo does not overlap with other logos/text.
  - Do not make the logo too small to be seen clearly.
  - Check that the logo has a clear contrast to the background.

Never distort or change the icon, font or colors of any of the logos.

# Logo Overview

| Description | transparent | white |
| ----------- | ---- | --- |
| logo         | <img src="logo/logo_transparent.png" height="150"/> | <img src="logo/logo_white.png" height="150"/> |
| logo inverse | <img src="logo_inverse/logo_inverse_transparent.png" height="150"/> | <img src="logo_inverse/logo_inverse_white.png" height="150"/> |
| logo full         | <img src="logo/logo_full_transparent.png" height="75"/> | <img src="logo/logo_full_white.png" height="75"/> |
| logo inverse full | <img src="logo_inverse/logo_inverse_full_transparent.png" height="75"/> | <img src="logo_inverse/logo_inverse_full_white.png" height="75"/> |

